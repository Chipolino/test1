﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TankSpace;
using _111;

namespace TankSpace

{
    class Gun
    {
    
        
        public static int MaxShot = 100;
        
        public static int CountShot = MaxShot;
        

        private DirectionEnum DirectionGun;
        private readonly int [] ArrayGan = new int[2];
       
        public Gun(DirectionEnum DirectionGun, int Left, int Top)
        {
            
                this.DirectionGun = DirectionGun;
                ArrayGan[0] = Left;
                ArrayGan[1] = Top;
                CountShot--;
                
            MoveGun();///////////////////// in Move

            if (CountShot==0 | !Karta.KartaHaveWall())
            {
                TankMainClass.FlagWeInProgram = false;
            }
                
     
        }

        
        public void MoveGun()   //Move
        {
            switch (DirectionGun)
            {
                case DirectionEnum.Up:
                    {
                        for (int i = ArrayGan[1]; i > Karta.MinTop; i--)
                        {
                            ArrayGan[1] -= 1;
                            Karta.ClearKartaFromGun();
                            Karta.GlobalCoordinate[ArrayGan[0], ArrayGan[1]] = Karta.GunView ;
                            Karta.DrawGun();
                        }
                        break;
                    }
                case DirectionEnum.Right:
                    {
                        for (int i = ArrayGan[0]; i < Karta.MaxLeft-1  ; i++)
                        {
                            ArrayGan[0] += 1;
                            Karta.ClearKartaFromGun();
                            Karta.GlobalCoordinate[ArrayGan[0], ArrayGan[1]] = Karta.GunView;
                            Karta.DrawGun();
                        }

                        break;
                    }
                case DirectionEnum.Down:
                    {
                        for (int i = ArrayGan[1]; i < Karta.MaxTop-1 ; i++)
                        {
                            ArrayGan[1] += 1;
                            Karta.ClearKartaFromGun();
                            Karta.GlobalCoordinate[ArrayGan[0], ArrayGan[1]] = Karta.GunView;
                            Karta.DrawGun();
                        }
                        break;
                    }
                case DirectionEnum.Left:
                    {
                        for (int i = ArrayGan[0]; i >  Karta.MinLeft ; i--)
                        {
                            ArrayGan[0] -= 1;
                            Karta.ClearKartaFromGun();
                            Karta.GlobalCoordinate[ArrayGan[0], ArrayGan[1]] = Karta.GunView;
                            Karta.DrawGun();
                        }
                        
                        break;
                    }
            }

            Karta.ClearKartaFromGun();
            Karta.DrawGun();


        }

    }
}
